package com.latudio.imagedownloader

import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

abstract class ImageDownloaderBase : IImageDownloader {

    protected abstract fun parseSrc(src: String): String

    protected open fun downloadPage(url: String): Connection.Response {
        return Jsoup.connect(url).ignoreContentType(true).ignoreHttpErrors(true).maxBodySize(Integer.MAX_VALUE).execute()
    }

    protected open fun parsePage(page: String): Document {
        return Jsoup.parse(page)
    }

}