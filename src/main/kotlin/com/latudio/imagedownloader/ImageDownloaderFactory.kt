package com.latudio.imagedownloader

import com.latudio.imagedownloader.downloader.FlickrDownloader
import com.latudio.imagedownloader.downloader.FreeimagesDownloader
import com.latudio.imagedownloader.downloader.PixabayDownloader
import com.latudio.imagedownloader.downloader.UnsplashDownloader

class ImageDownloaderFactory : IImageDownloaderFactory {

    override fun getDownloader(url: String): IImageDownloader {
        return when {
            url.contains("flickr") -> FlickrDownloader()
            url.contains("freeimages") -> FreeimagesDownloader()
            url.contains("pixabay") -> PixabayDownloader()
            url.contains("unsplash") -> UnsplashDownloader()
            else -> throw NotImplementedError()
        }
    }
}