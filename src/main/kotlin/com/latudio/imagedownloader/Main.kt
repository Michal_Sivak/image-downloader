package com.latudio.imagedownloader

import java.nio.file.Files
import java.nio.file.Paths


fun main(args: Array<String>) {

//    http://www.freeimages.com/photo/people-1-1465030
//    https://pixabay.com/en/chaos-clutter-a-mess-things-stuff-227971/
//    https://unsplash.com/photos/Vo52cKzOxMY
//    https://www.flickr.com/photos/seanfreese/6873738692/in/photolist-btpH35-7CdcW5-6szmvc-PLBg7-7CzRpf-9svyMV-otyTA2-p6on8V-9bMi7H-bPR15p-bE8eQ8-uRNwP-cgEtVb-6Rykig-7D2WeM-83soDw-93pMK2-uRPdR-7uFo3f-6UQ2QD-8snfvt-8cF4Q1-8aAiRQ-4yJUwU-7C89uT-o5LFPa-83Va4Q-e24xkH-5UH6gR-cS5qLu-uRNXh-6KTpPw-4dSgKg-aFue3p-7YGVe2-ejtbCF-ejtbrV-dP2PDM-55CRAv-4dYkco-6i41Vu-uRMZn-pCU42P-6mGqRs-zeNi-7ENso7-4yo8gF-7oTJYz-gXYMig-fvqJKk


    val testUrl = "https://www.flickr.com/photos/seanfreese/6873738692/in/photolist-btpH35-7CdcW5-6szmvc-PLBg7-7CzRpf-9svyMV-otyTA2-p6on8V-9bMi7H-bPR15p-bE8eQ8-uRNwP-cgEtVb-6Rykig-7D2WeM-83soDw-93pMK2-uRPdR-7uFo3f-6UQ2QD-8snfvt-8cF4Q1-8aAiRQ-4yJUwU-7C89uT-o5LFPa-83Va4Q-e24xkH-5UH6gR-cS5qLu-uRNXh-6KTpPw-4dSgKg-aFue3p-7YGVe2-ejtbCF-ejtbrV-dP2PDM-55CRAv-4dYkco-6i41Vu-uRMZn-pCU42P-6mGqRs-zeNi-7ENso7-4yo8gF-7oTJYz-gXYMig-fvqJKk"

    val downloader = ImageDownloaderFactory().getDownloader(testUrl)
    val img = downloader.getImageStream(testUrl)

    Files.deleteIfExists(Paths.get("/Users/system/Downloads/test.jpg"))

    Files.copy(img, Paths.get("/Users/system/Downloads/test.jpg"))
}

