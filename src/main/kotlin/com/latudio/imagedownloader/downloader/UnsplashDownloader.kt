package com.latudio.imagedownloader.downloader

import com.latudio.imagedownloader.ImageDownloaderBase
import java.io.ByteArrayInputStream
import java.io.InputStream

class UnsplashDownloader : ImageDownloaderBase() {
    override fun getImageStream(pageUrl: String): InputStream {
        val page = downloadPage(pageUrl)
        val document = parsePage(page.body())

        val imgTag = document.select("div._2yFK-.IEpfq > img").attr("srcset")
        val imgUrl = parseSrc(imgTag)

        return ByteArrayInputStream(downloadPage(imgUrl).bodyAsBytes())
    }

    override fun parseSrc(src: String): String {
        val sources: List<Pair<Double, String>> = src.split(", ").map { it ->
            val regex = Regex("((http|ftp|https):\\/\\/([\\w_-]+(?:(?:\\.[\\w_-]+)+))([\\w.,@?^=%&:\\/~+#-]*[\\w@?^=%&\\/~+#-])?).([0-9]+)").find(it)
            return@map Pair(regex?.groupValues?.get(5)?.toDouble() ?: 0.0, regex?.groupValues?.get(1) ?: "")
        }

        var max = sources[0]
        sources.forEach { it ->
            if (it.first >= max.first) max = it
        }

        return max.second
    }

}