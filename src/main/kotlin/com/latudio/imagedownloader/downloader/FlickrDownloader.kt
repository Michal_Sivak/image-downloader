package com.latudio.imagedownloader.downloader

import com.latudio.imagedownloader.ImageDownloaderBase
import java.io.*
import java.net.URL

class FlickrDownloader : ImageDownloaderBase() {
    override fun getImageStream(pageUrl: String): InputStream {
        val page = downloadPage(pageUrl)

        val document = parsePage(page.body())
        val imgUrl = document.select("meta[property=og:image]").attr("content")

        return ByteArrayInputStream(URL(imgUrl).openStream().readBytes())
    }

    override fun parseSrc(src: String): String {
        TODO("Not needed")
    }
}