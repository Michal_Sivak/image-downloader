package com.latudio.imagedownloader.downloader

import com.latudio.imagedownloader.ImageDownloaderBase
import java.io.InputStream
import java.io.ByteArrayInputStream


class PixabayDownloader : ImageDownloaderBase() {
    override fun getImageStream(pageUrl: String): InputStream {
        val page = downloadPage(pageUrl)
        val document = parsePage(page.body())

        val imgTag = document.select("#media_container > img").attr("srcset")
        val imgUrl = parseSrc(imgTag)

        return ByteArrayInputStream(downloadPage(imgUrl).bodyAsBytes())
    }

    override fun parseSrc(src: String): String {
        val sources: List<Pair<Double, String>> = src.split(", ").map { it ->
            val regex = Regex("((http|ftp|https):\\/\\/([\\w_-]+(?:(?:\\.[\\w_-]+)+))([\\w.,@?^=%&:\\/~+#-]*[\\w@?^=%&\\/~+#-])?).([1-9].+)").find(it)
            return@map Pair(regex?.groupValues?.get(5)?.replace("x", "")?.toDouble() ?: 0.0, regex?.groupValues?.get(1) ?: "")
        }

        var max = sources[0]
        sources.forEach { it ->
            if (it.first >= max.first) max = it
        }

        return max.second
    }
}