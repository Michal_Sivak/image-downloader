package com.latudio.imagedownloader.downloader

import com.latudio.imagedownloader.ImageDownloaderBase
import org.jsoup.Connection
import org.jsoup.Jsoup
import java.io.ByteArrayInputStream
import java.io.InputStream

class FreeimagesDownloader : ImageDownloaderBase() {
    override fun getImageStream(pageUrl: String): InputStream {
        val page = downloadPage(pageUrl)
        val document = parsePage(page.body())

        val imgUrl = document.select("#content > div > div.display-table.listing-body > div.table-cell.main > div > div.detail-preview > div > a > img").attr("src")

        return ByteArrayInputStream(downloadPage(imgUrl).bodyAsBytes())
    }

    override fun parseSrc(src: String): String = ""

    override fun downloadPage(url: String): Connection.Response {
        return Jsoup.connect(url).ignoreContentType(true).ignoreHttpErrors(true).timeout(50000).maxBodySize(Integer.MAX_VALUE).execute()
    }
}