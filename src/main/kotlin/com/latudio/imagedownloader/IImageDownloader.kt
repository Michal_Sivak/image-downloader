package com.latudio.imagedownloader

import java.io.InputStream

interface IImageDownloader {
    /**
     * Downloads image at given page and returns its input stream.
     *
     * @param pageUrl
     * @param file
     * @return input stream of the image that is on the page
     */
    fun getImageStream(pageUrl: String): InputStream


}